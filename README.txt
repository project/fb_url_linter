
This module is well used for cases when Facebook can't correctly fetch 
information about you site content that are used for basic Share link dialog.

You can send your content to Facebook URL Linter service on node form 
submission or simply by cron.

Installation
------------
Copy module folder into modules folder of your Drupal installation 
(i.e. sites/all/modules) and enable module on Modules administration page. 
On module configuration page admin/config/search/facebook-url-linter specify 
which content-types will be processed by this module.

Author
------
Tomas Barej
tomasbarej@gmail.com
